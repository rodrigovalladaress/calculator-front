import Vue from "vue";
import Calculator from "@/components/Calculator/Calculator.vue";

import utils from "../lib/utils";
const { escapeDOMQuery, clickButtonsByString } = utils;

const buttons = [
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  0,
  "+",
  "-",
  "*",
  "/",
  "=",
  { id: "C", action: "clear" }
];
// const operation = [1, "+", 3, "="];

describe("Calculator.vue", () => {
  const Constructor = Vue.extend(Calculator);
  const vm = new Constructor({
    propsData: {
      buttons
    }
  }).$mount();

  // Check if buttons are being rendered
  for (const button of buttons) {
    it(`should render button "${
      typeof button === "object" ? button.id : button
    }"`, () => {
      const buttonText = typeof button === "object" ? button.id : button;
      const id = escapeDOMQuery(`#calculator-button-${buttonText}`);
      const buttonTextContent = vm.$el.querySelector(id).textContent.trim();
      expect(buttonTextContent).to.equal(`${buttonText}`);
    });
  }

  // Check if the operation text is being rendered
  const textEl = vm.$el.querySelector("#calculator-text");

  it("should show text 1+3", done => {
    clickButtonsByString(vm, "1+3");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("1+3");
      done();
    }, 1);
  });

  // Check if calculator clears text
  it("should clear text", done => {
    clickButtonsByString(vm, "C");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("");
      done();
    }, 1);
  });

  // Check addition
  it("should calculate 2+5", done => {
    clickButtonsByString(vm, "C2+5=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("7");
      done();
    }, 1);
  });

  it("should calculate 2+5+31", done => {
    clickButtonsByString(vm, "C2+5+31=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("38");
      done();
    }, 1);
  });

  // Check substraction
  it("should calculate 5-2", done => {
    clickButtonsByString(vm, "C5-2=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("3");
      done();
    }, 1);
  });

  it("should calculate 2-5-7", done => {
    clickButtonsByString(vm, "C2-5-7=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("-10");
      done();
    }, 1);
  });

  // Check multiplication
  it("should calculate 2*3", done => {
    clickButtonsByString(vm, "C2*3=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("6");
      done();
    }, 1);
  });

  it("should calculate 2*8*10", done => {
    clickButtonsByString(vm, "C2*8*10=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("160");
      done();
    }, 1);
  });

  // Check division
  it("should calculate 4/2", done => {
    clickButtonsByString(vm, "C4/2=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("2");
      done();
    }, 1);
  });

  it("should calculate 24/5/2", done => {
    clickButtonsByString(vm, "C24/5/2=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("2.4");
      done();
    }, 1);
  });

  // Check errors
  it("should show an error for \"1+-3=\"", done => {
    clickButtonsByString(vm, "1+-3=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("ERROR");
      done();
    }, 1);
  });

  it("should show an error for \"+1=\"", done => {
    clickButtonsByString(vm, "C+1=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("ERROR");
      done();
    }, 1);
  });

  it("should show an error for \"1+=\"", done => {
    clickButtonsByString(vm, "C1+=");
    setTimeout(() => {
      expect(textEl.textContent.trim()).to.equal("ERROR");
      done();
    }, 1);
  });
});
