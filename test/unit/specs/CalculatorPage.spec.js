/* eslint no-unused-expressions: 0 */
import Vue from "vue";
import CalculatorPage from "@/pages/CalculatorPage";

import utils from "../lib/utils";
const { escapeDOMQuery } = utils;

const buttons = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "+", "-", "*", "/", "="];

describe("CalculatorPage.vue", () => {
  const Constructor = Vue.extend(CalculatorPage);
  const vm = new Constructor().$mount();

  it("should render calculator", () => {
    expect(vm.$el.querySelector("#calculator")).to.exist;
  });

  // Should render basic operations buttons
  for (const button of buttons) {
    let id = escapeDOMQuery(`#calculator-button-${button}`);
    it(`should render button ${button} (${id})`, () => {
      expect(vm.$el.querySelector(id)).to.exist;
    });
  }
});
