import Vue from "vue";
import CalculatorButton from "@/components/Calculator/CalculatorButton/CalculatorButton.vue";

const icons = ["1", 2, "+", "delete", ""];

describe("CalculatorButton.vue", () => {
  const Constructor = Vue.extend(CalculatorButton);

  for (const icon of icons) {
    it(`should render icon "${icon}"`, () => {
      const vm = new Constructor({
        propsData: {
          icon
        }
      }).$mount();
      expect(vm.$el.textContent.trim()).to.equal(`${icon}`);
    });
  }
});
