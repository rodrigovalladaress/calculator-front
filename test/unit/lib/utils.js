const escapeDOMQuery = (query = "") => {
  // Some need to be scaped or querySelector fails
  return query
    .replace("+", "\\+")
    .replace("*", "\\*")
    .replace("/", "\\/")
    .replace("=", "\\=");
};

const sleepAsync = (ms = 1) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

const clickButtonsByString = (vm, str) => {
  for (const char of str) {
    clickButton(vm, escapeDOMQuery(`#calculator-button-${char}`));
  }
};

const clickButton = (vm, query) => {
  const button = vm.$el.querySelector(query);
  try {
    button.click();
  } catch (err) {
    throw new Error(`button with query "${query}" not found`);
  }
};

export default {
  escapeDOMQuery,
  sleepAsync,
  clickButtonsByString,
  clickButton
};
