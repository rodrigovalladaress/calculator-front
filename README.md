# calculator-front

A Vue.js simple project to practise TDD. 
Basically, it's a calculator that:

- Gets an operation on infix notation
- Transform it to postfix using the shunting yard algorithm
- Evaluates the postfix operation using a stack

![picture](sample.PNG)

## Build Setup (vue.js template)

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
