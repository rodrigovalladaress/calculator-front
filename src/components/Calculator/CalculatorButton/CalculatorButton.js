export default {
  name: "CalculatorButton",

  props: {
    icon: {
      type: String | Number,
      default: ""
    }
  },

  methods: {
    onClick () {
      this.$emit("calculator-button-click", this.icon);
    }
  }
};
