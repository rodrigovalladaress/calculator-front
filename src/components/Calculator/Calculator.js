import CalculatorButton from "./CalculatorButton/CalculatorButton.vue";

// Constants for actions for buttons.
const PRINT = "print";
const CALCULATE = "calculate";
const CLEAR = "clear";

const PRECEDENCE = Object.freeze({
  "*": 3,
  "/": 3,
  "+": 2,
  "-": 2
});

let buttonsActions = {};

export default {
  components: { CalculatorButton },

  data() {
    return {
      rows: [["/", "*", "-", "+"]],
      buttons: [
        "/",
        "*",
        "-",
        "+",
        7,
        8,
        9,

        { id: "=", action: "calculate" },
        4,
        5,
        6,
        1,
        2,
        3,
        0,
        { id: "C", action: "clear" }
      ],
      text: ""
    };
  },

  methods: {
    getIcon(button) {
      return typeof button === "object" ? button.id : button;
    },

    onButtonClick(button) {
      if (this.text === "ERROR") {
        this.text = "";
      }

      switch (buttonsActions[button]) {
        case CALCULATE:
          this.text = this.calculateResult();
          break;
        case CLEAR:
          this.text = "";
          break;
        default:
          this.text += button;
          break;
      }
    },

    // Calculate postfix expressiont using shunting yard.
    shuntingYard() {
      // Stack of operators
      let opStack = [];
      // Array of outputs
      let outPut = ["0"];

      for (const char of this.text) {
        const last = outPut.length - 1;
        if (!isNaN(char)) {
          // If it is a number,...
          if (!isNaN(outPut[last])) {
            // ... append the number char to the last item of outPut if it's a
            // number.
            outPut[last] += char;
          } else {
            // ... push the number to a new item of outPut if it's not a number.
            outPut.push(char);
          }
        } else {
          // If it is an operator...
          // ... parseFloat the last outPut (it's always a number)
          if (!isNaN(outPut[last])) {
            outPut[last] = parseFloat(outPut[last]);
          }
          let lastOp = opStack.length - 1;
          const currentPrecedence = PRECEDENCE[char];
          // Pop from stack to output if the top operator has more precedence.
          while (
            lastOp >= 0 &&
            PRECEDENCE[opStack[lastOp]] >= currentPrecedence
          ) {
            outPut.push(opStack.pop());
            lastOp--;
          }
          // Push the char to the stack and the new number
          opStack.push(char);
          outPut.push("0");
        }
      }

      const last = outPut.length - 1;
      if (!isNaN(outPut[last])) {
        outPut[last] = parseFloat(outPut[last]);
      }
      for (let i = opStack.length - 1; i >= 0; i--) {
        outPut.push(opStack[i]);
      }

      return outPut;
    },

    calculateOp(a, b, op) {
      let result = 0;
      switch (op) {
        case "+":
          result = a + b;
          break;
        case "-":
          result = a - b;
          break;
        case "*":
          result = a * b;
          break;
        case "/":
          result = a / b;
          break;
        default:
      }
      return result;
    },

    calculateResult() {
      const regex = /^(\d+)($|([+\-*/])(\d+))+/;
      if (this.text.match(regex)) {
        const postfix = this.shuntingYard();
        const numsStack = [];

        // Calculate postfix expression
        for (const char of postfix) {
          if (!isNaN(char)) {
            numsStack.push(char);
          } else {
            const a = numsStack.pop();
            const b = numsStack.pop();
            numsStack.push(this.calculateOp(b, a, char));
          }
        }
        return numsStack[0];
      } else {
        return "ERROR";
      }
    }
  },

  created() {
    for (const button of this.buttons) {
      if (typeof button === "string" || typeof button === "number") {
        buttonsActions[button] = PRINT;
      } else if (typeof button === "object") {
        buttonsActions[button.id] = button.action;
      }
    }
  }
};
