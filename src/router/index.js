import Vue from "vue";
import Router from "vue-router";
// import HelloWorld from "@/components/HelloWorld";
import CalculatorPage from "@/pages/CalculatorPage.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "CalculatorPage",
      component: CalculatorPage
    }
  ]
});
